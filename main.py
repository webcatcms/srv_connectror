import configparser
import socket
import struct
import pickle
from model import Model


class SrvConnector():

    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read('config.ini')
        self.model = Model(self.config)
        self.conn = socket.socket()
        self.conn.bind(("", int(self.config['server']['port'])))
        self.conn.listen()
        self.BUFFER_SIZE = 4096
        self.is_alive = True

    def main(self):
        while True:
            try:
                Client, address = self.conn.accept()
                while True:
                    try:
                        data_size = struct.unpack('>I', Client.recv(4))[0]
                        received_payload = b""
                        reamining_payload_size = data_size
                        while reamining_payload_size != 0:
                            received_payload += Client.recv(reamining_payload_size)
                            reamining_payload_size = data_size - len(received_payload)
                        data = pickle.loads(received_payload)
                    except Exception as e:
                        pass

                        if not data:
                            break

                    if 'get_config' in data:

                        config = self.model.get_config(data['get_config'])
                        if config != None:
                            response = {'config': config[2].tobytes(), 'login': config[0], 'password': config[1]}
                        else:
                            response = {'config': None}
                        data_send = pickle.dumps(response)

                        Client.sendall(struct.pack('>I', len(data_send)))
                        Client.sendall(data_send)

            except Exception as e:
                pass

        self.conn.close()


if __name__ == '__main__':
    server = SrvConnector()
    server.main()
