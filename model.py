import os
import psycopg2
import datetime

class Model(object):
    _instance = None

    def __new__(cls, config):
        if cls._instance is None:
           cls._instance = super(Model, cls).__new__(cls)
        return cls._instance

    def __init__(self, config):

        try:
            self.conn = psycopg2.connect(
                database=config['database']['dbname'],
                user=config['database']['username'],
                password=config['database']['password'],
                host=config['database']['hostname'],
                port=config['database']['port'],)
            self.cursor = self.conn.cursor()
        except Exception as e:
            pass


    def get_config(self, edrpo):

       self.cursor.execute('select c.login, c.password, c.file from org o inner join config c on(o.id_config=c.id) where edrpo = %s ', (edrpo,))
       data = self.cursor.fetchone()

       return data


